<?php

namespace Dropkick\Core\Invokable;

use PHPUnit\Framework\TestCase;

class ResolverTest extends TestCase {

  public function testResolver() {
    $resolver = new Resolver();
    $resolver->addResolver(new TestResolver());

    $argument = new Argument(0, '', 'item', true, 'default');
    $this->assertEquals(true, $resolver->applies($argument));
    $this->assertEquals('default', $resolver->getValue($argument));

    $argument = new Argument(0, '', 'item', false, null);
    $this->assertEquals(false, $resolver->applies($argument));

    $argument = new Argument(0, '', 'test', true, 'default');
    $this->assertEquals(true, $resolver->applies($argument));
    $this->assertEquals('test', $resolver->getValue($argument));
  }

}

class TestResolver implements ResolverInterface {
  public function applies(ArgumentInterface $argument) {
    return $argument->getName() === 'test';
  }

  public function getValue(ArgumentInterface $argument) {
    if ($argument->getName() === 'test') {
      return 'test';
    }
    return 'value';
  }
}
