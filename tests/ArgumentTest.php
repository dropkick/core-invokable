<?php

namespace Dropkick\Core\Invokable;

use PHPUnit\Framework\TestCase;

class ArgumentTest extends TestCase {

  public function testArgument() {
    $pos = 2;
    $type = 'type';
    $name = 'name';
    $has_default = false;
    $default = 'default';

    $argument = new Argument($pos, $type, $name, $has_default, $default);

    $this->assertEquals($pos, $argument->getPosition());
    $this->assertEquals($type, $argument->getType());
    $this->assertEquals($name, $argument->getName());
    $this->assertEquals($has_default, $argument->hasDefault());
    $this->assertEquals(NULL, $argument->getDefault());
  }

  public function testDefault() {
    $pos = 3;
    $type = 'type';
    $name = 'name';
    $has_default = true;
    $default = 'default';

    $argument = new Argument($pos, $type, $name, $has_default, $default);

    $this->assertEquals($pos, $argument->getPosition());
    $this->assertEquals($type, $argument->getType());
    $this->assertEquals($name, $argument->getName());
    $this->assertEquals($has_default, $argument->hasDefault());
    $this->assertEquals($default, $argument->getDefault());
  }

}
