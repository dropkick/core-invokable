<?php

namespace Dropkick\Core\Invokable;

use PHPUnit\Framework\TestCase;

class ReflectorTest extends TestCase {

  public function testInvoke() {
    $reflector = new Reflector();

    $arguments = $reflector->getArguments(new TestInvokeClass());
    $this->assertEquals(0, count($arguments));

    $arguments = $reflector->getArguments(new TestInvokeArgClass());
    $this->assertEquals(2, count($arguments));
    $this->assertEquals('test', $arguments[0]->getName());
    $this->assertEquals(false, $arguments[0]->hasDefault());
    $this->assertEquals('item', $arguments[1]->getName());
    $this->assertEquals(true, $arguments[1]->hasDefault());
  }

  public function testConstructor() {
    $reflector = new Reflector();

    $arguments = $reflector->getArguments([TestMethodClass::class, '__construct']);
    $this->assertEquals(2, count($arguments));
    $this->assertEquals('test', $arguments[0]->getName());
    $this->assertEquals(false, $arguments[0]->hasDefault());
    $this->assertEquals('item', $arguments[1]->getName());
    $this->assertEquals(true, $arguments[1]->hasDefault());
  }

  public function testMethod() {
    $reflector = new Reflector();

    $arguments = $reflector->getArguments([TestMethodClass::class, 'method']);
    $this->assertEquals(2, count($arguments));
    $this->assertEquals('test', $arguments[0]->getName());
    $this->assertEquals(false, $arguments[0]->hasDefault());
    $this->assertEquals('item', $arguments[1]->getName());
    $this->assertEquals(true, $arguments[1]->hasDefault());
  }

  public function testFunction() {
    $reflector = new Reflector();

    $arguments = $reflector->getArguments('\Dropkick\Core\Invokable\testFunction');
    $this->assertEquals(2, count($arguments));
    $this->assertEquals('test', $arguments[0]->getName());
    $this->assertEquals(false, $arguments[0]->hasDefault());
    $this->assertEquals('item', $arguments[1]->getName());
    $this->assertEquals(true, $arguments[1]->hasDefault());
  }

  public function testClosure() {
    $reflector = new Reflector();

    $arguments = $reflector->getArguments(function($test, $item = NULL) {});
    $this->assertEquals(2, count($arguments));
    $this->assertEquals('test', $arguments[0]->getName());
    $this->assertEquals(false, $arguments[0]->hasDefault());
    $this->assertEquals('item', $arguments[1]->getName());
    $this->assertEquals(true, $arguments[1]->hasDefault());
  }

  public function testInvalid() {
    $reflector = new Reflector();

    $arguments = $reflector->getArguments(NULL);
    $this->assertEquals(0, count($arguments));
  }

  public function testNoConstructor() {
    $reflector = new Reflector();

    $arguments = $reflector->getArguments([TestInvokeClass::class, '__construct']);
    $this->assertEquals(0, count($arguments));
  }
}

class TestInvokeClass {
  public function __invoke() {
  }
}

class TestInvokeArgClass {
  public function __invoke($test, $item = NULL) {
  }
}

class TestMethodClass {
  public function __construct($test, $item = NULL) {
  }

  public function method($test, $item = NULL) {
  }
}

function testFunction($test, $item = NULL) {
}