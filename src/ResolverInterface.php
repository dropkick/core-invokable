<?php

namespace Dropkick\Core\Invokable;

/**
 * Interface ResolverInterface.
 *
 * Allow for arguments to be resolved in different ways.
 */
interface ResolverInterface {

  /**
   * Confirm the resolver applies for the argument.
   *
   * @param \Dropkick\Core\Invokable\ArgumentInterface $argument
   *   The argument to be resolved.
   *
   * @return bool
   *   The confirmation.
   */
  public function applies(ArgumentInterface $argument);

  /**
   * Confirm the resolver applies for the argument.
   *
   * @param \Dropkick\Core\Invokable\ArgumentInterface $argument
   *   The argument to be resolved.
   *
   * @return mixed
   *   The value for the argument.
   */
  public function getValue(ArgumentInterface $argument);

}
