<?php

namespace Dropkick\Core\Invokable;

/**
 * Class Reflector.
 *
 * A generic implementation of ReflectorInterface.
 */
class Reflector implements ReflectorInterface {

  /**
   * {@inheritdoc}
   */
  public function getArguments($callable) {
    $arguments = [];
    foreach ($this->getReflectionParameters($callable) as $pos => $param) {
      $name = $param->getName();
      $type = $param->getType();
      $has_default = $param->isDefaultValueAvailable();
      $default = $has_default ? $param->getDefaultValue() : NULL;

      // Create the argument from the reflected parameters.
      $arguments[] = $this->getArgument($pos, $type, $name, $has_default, $default);
    }
    return $arguments;
  }

  /**
   * Get the parameters for the callable.
   *
   * @param mixed $callable
   *   The callable.
   *
   * @return \ReflectionParameter[]
   *   The reflected parameter list.
   */
  protected function getReflectionParameters($callable) {
    // A closure.
    if ($callable instanceof \Closure) {
      return (new \ReflectionFunction($callable))->getParameters();
    }
    // An invokable class.
    if (is_object($callable) && method_exists($callable, '__invoke')) {
      return (new \ReflectionMethod($callable, '__invoke'))->getParameters();
    }
    // The standard object -> method callable.
    if (is_array($callable) && count($callable) === 2) {
      // Handle the case when the constructor does not exist for the class.
      if ($callable[1] === '__construct' && !(new \ReflectionClass($callable[0]))->getConstructor()) {
        return [];
      }
      return (new \ReflectionMethod($callable[0], $callable[1]))->getParameters();
    }
    // A function call.
    if (is_string($callable) && function_exists($callable)) {
      return (new \ReflectionFunction($callable))->getParameters();
    }
    return [];
  }

  /**
   * Get the argument as a class.
   *
   * @param int $pos
   *   The argument position.
   * @param string $type
   *   The type hint of the argument.
   * @param string $name
   *   The name of the argument.
   * @param bool $has_default
   *   Flag for default value.
   * @param mixed $default
   *   The default value.
   *
   * @return \Dropkick\Core\Invokable\ArgumentInterface
   *   An argument instance.
   */
  protected function getArgument($pos, $type, $name, $has_default, $default) {
    return new Argument($pos, $type, $name, $has_default, $default);
  }

}
