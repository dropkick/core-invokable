<?php

namespace Dropkick\Core\Invokable;

/**
 * Interface ReflectorInterface.
 *
 * Allow callables to be reflected into arguments that can then be resolved
 * using ResolverInterface.
 */
interface ReflectorInterface {

  /**
   * Get the arguments used by a callable.
   *
   * @param mixed $callable
   *   The callable to be resolved. A callable may include constructor, which
   *   cannot be called statically, so will fail the callable type hint.
   *
   * @return \Dropkick\Core\Invokable\ArgumentInterface[]
   *   The arguments.
   */
  public function getArguments($callable);

}
