<?php

namespace Dropkick\Core\Invokable;

/**
 * Interface ArgumentInterface.
 *
 * Allows for definition of an argument via reflection.
 */
interface ArgumentInterface {

  /**
   * Return the position of the argument.
   *
   * @return int
   *   The position.
   */
  public function getPosition();

  /**
   * Return the type of argument.
   *
   * @return string
   *   The argument type.
   */
  public function getType();

  /**
   * Return the name of the argument.
   *
   * @return string
   *   The argument name.
   */
  public function getName();

  /**
   * Confirm the argument has a default value.
   *
   * @return bool
   *   The confirmation.
   */
  public function hasDefault();

  /**
   * The default value for the argument.
   *
   * @return mixed
   *   The default value.
   */
  public function getDefault();

}
