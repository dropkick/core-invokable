<?php

namespace Dropkick\Core\Invokable;

/**
 * Class Argument.
 *
 * A generic implementation of ArgumentInterface.
 */
class Argument implements ArgumentInterface {

  /**
   * The argument position.
   *
   * @var int
   */
  protected $position;

  /**
   * The argument type hint.
   *
   * @var string
   */
  protected $type;

  /**
   * The argument name.
   *
   * @var string
   */
  protected $name;

  /**
   * The argument has a default value.
   *
   * @var bool
   */
  protected $hasValue;

  /**
   * The argument default value.
   *
   * @var mixed
   */
  protected $default;

  /**
   * Argument constructor.
   *
   * @param int $position
   *   The argument position.
   * @param string $type
   *   The argument type.
   * @param string $name
   *   The argument name.
   * @param bool $has_default
   *   The argument supports a default value.
   * @param mixed $default
   *   The argument default value.
   */
  public function __construct($position, $type, $name, $has_default, $default) {
    $this->position = (int) $position;
    $this->type = (string) $type;
    $this->name = (string) $name;
    $this->hasValue = (bool) $has_default;
    $this->default = $default;
  }

  /**
   * {@inheritdoc}
   */
  public function getPosition() {
    return $this->position;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function hasDefault() {
    return $this->hasValue;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefault() {
    return $this->hasValue ? $this->default : NULL;
  }

}
