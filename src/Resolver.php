<?php

namespace Dropkick\Core\Invokable;

/**
 * Class Resolver.
 *
 * A generic implementation of ResolverInterface.
 */
class Resolver implements ResolverInterface {

  /**
   * The resolvers that can handle the arguments.
   *
   * @var \Dropkick\Core\Invokable\ResolverInterface[]
   */
  protected $resolvers = [];

  /**
   * Add a resolver to the resolvers.
   *
   * @param \Dropkick\Core\Invokable\ResolverInterface $resolver
   *   A resolver object.
   *
   * @return static
   *   The resolver object.
   */
  public function addResolver(ResolverInterface $resolver) {
    $this->resolvers[] = $resolver;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(ArgumentInterface $argument) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->applies($argument)) {
        return TRUE;
      }
    }
    return $argument->hasDefault();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ArgumentInterface $argument) {
    foreach ($this->resolvers as $resolver) {
      if ($resolver->applies($argument)) {
        return $resolver->getValue($argument);
      }
    }
    return $argument->hasDefault() ? $argument->getDefault() : NULL;
  }

}
